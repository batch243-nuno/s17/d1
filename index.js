// console.log(`Hello Batch 243!`);

// Function
// Function Declaration
function printName(){
    console.log(`My name is John.`);
    console.log(`My last name is Dela Cruz`);
}
// function invocation
printName();
// Function Declaration is hoisted
declaredFunction();
function declaredFunction(){
    console.log(`Hello World from declaredFunction!`);
}

//Function Expression
// Anonymous Function - functions without name
// variableFunction();

let variableFunction = function(){
    console.log(`Hello World from variableFunction!`);
}
variableFunction();

let funcExpression = function funcName(){
    console.log(`Hello World from funcExpression!`);
}
funcExpression();

// Reassigning
declaredFunction = function(){
    console.log(`Updated declaredFunction`);
}
declaredFunction();

funcExpression = function(){
    console.log(`Updated funcExpression`);
}
funcExpression();

// Function Expression using const
    // cannot be reaasign
const constantFunc = function(){
    console.log(`Initialized with const!`);
}
constantFunc();

// Function Scoping

// local Scope
{
    let localVar = "Armando Perez";
    console.log(localVar);
}
// Global Scope
let globalVar = "Mr. Worldwide";

// Function Scope
function showNames(){
    let functionLet = `Jane`;
    const functionConst = `John`;

    console.log(functionConst);
    console.log(functionLet);
}
showNames();

// Nexted Functions
function myNewFunction(){
    let name = `Jane`;
    console.log(name);
     function nestedFunction(){
        let nestedName = `John`;

        console.log(nestedName);
        console.log(name);
    }
    nestedFunction();
}
myNewFunction();

// Function and Global Scope Variables
// Global Scope
let globalName = `Alexandro`;

function myNewFunction2(){
    let nameInside = `Renz`;

    console.log(globalName);
    console.log(nameInside);
}
myNewFunction2();

// Alert
// alert(`Hello World`);

// function showSampleAlert(){
//     let myName = `Jek`;
//     alert(`Hello, User ${myName}!`);
// }
// showSampleAlert();

// // Prompt

// let samplePrompt = prompt("Enter your name: ")

// console.log(alert(`Hello, User ${samplePrompt}!`));

// function printWelcomeMessages(){
//     let firstName = prompt(`Enter your First Name:`);
//     let lastName = prompt(`Enter your Last Name:`);

//     console.log(alert(`Hello, ${firstName} ${lastName}!`));
// }
// printWelcomeMessages();

// Function Naming Conversion
    // should be definitive / contain a verb

    function getCourses(){
        let course = ["Science 101", "Math 101", "English 101"];
        console.log(course);
    }
    getCourses();

    // avoid generic names
    // avoid pointless names
    // use camelCasing